package com.harshkalra.cardshuffler;

/**
 * Created by harsh.kalra on 3/6/14.
 */
public class Card {

    private CardSuit suit;
    private CardName cardName;
    private Float randomValue;   

    
    public Card(CardSuit suit, CardName cardName) {
        this.suit = suit;
        this.cardName = cardName;
    }

    //getters
    public CardSuit getSuit() {
        return suit;
    }
    
    public CardName getCardName() {
        return cardName;
    }
    
    public Float getRandomValue() {
        return randomValue;
    }

    //setters
    public void setSuit(CardSuit suit) {
        this.suit = suit;
    }
   
    public void setCardName(CardName cardName) {
        this.cardName = cardName;
    }   

    public void setRandomValue(Float randomValue) {
        this.randomValue = randomValue;
    }
    
    public String toString() {
        return cardName.getName() + " of " + suit.name();
    }

}
