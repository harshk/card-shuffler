package com.harshkalra.cardshuffler;

/**
 * Created by harsh.kalra on 3/6/14.
 */
public enum CardSuit {
    CLUBS,
    DIAMONDS,
    SPAIDS,
    HEARTS
}
