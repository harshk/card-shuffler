package com.harshkalra.cardshuffler;

/**
 * Created by harsh.kalra on 3/6/14.
 */
public enum CardName {
    ID_2("2"),
    ID_3("3"),
    ID_4("4"),
    ID_5("5"),
    ID_6("6"),
    ID_7("7"),
    ID_8("8"),
    ID_9("9"),
    ID_10("10"),
    ID_JACK("Jack"),
    ID_QUEEN("Queen"),
    ID_KING("King"),
    ID_ACE("Ace");

    String name;

    CardName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
