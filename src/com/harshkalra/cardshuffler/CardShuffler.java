package com.harshkalra.cardshuffler;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 * Created by harsh.kalra on 3/6/14.
 */
public class CardShuffler {
    
    private SecureRandom randomGenerator = new SecureRandom();
    private List<Card> cards = new ArrayList<Card>();
    
    /**
     * The constructor sets up the list of cards (unsorted)
     */
    public CardShuffler() {
        initCards();
    }

    /***
     * This method sets up the initial list of unsorted cards.
     */
    private void initCards() {
        for (CardSuit suit : CardSuit.values()) {
            for (CardName name : CardName.values()) {
                Card card = new Card(suit, name);
                cards.add(card);
            }
        }
    }
    
    /**
     * This method shuffles the cards using a common pattern in which each card is assigned a 
     * random float value.  The list is then sorted in ascending order by the randomValue property. 
     */
    private void shuffleCards() {
        for (Card card : cards) {
            card.setRandomValue(randomGenerator.nextFloat());
        }
        
        Collections.sort(cards, new Comparator<Card>() {
            @Override
            public int compare(Card o1, Card o2) {
                if (o1.getRandomValue() == o2.getRandomValue()) {
                    return 0;
                } else {
                    return o1.getRandomValue() < o2.getRandomValue() ? -1 : 1;
                }
            }
        });
    }
    
    /**
     * This method picks a random number between 0 - 51 and returns the
     * card at the randomly picked index.
     * @return the randomly selected card
     */
    private Card drawRandomCard() {
        return cards.get(randomGenerator.nextInt(cards.size()));
    }

    /**
     * This method iterates through cards and calls the toString method on each cards.  The title 
     * is used as a heading which is printed to the console before the list of cards.
     * @param title
     */
    private void printCards(String title) {
        System.out.println(title);
        System.out.println("--------------------------------");
        int counter = 1;
        for (Card card: cards) {
            System.out.println("[" + counter +"] : " + card);
            counter++;
        }
        System.out.println("--------------------------------\n\n");
    }


    public static void main(String [] args) {

        String question = "Press 1 to draw a random card.\nPress 2 to shuffle the deck\nPress 3 to exit.\n";

        Scanner scanner = new Scanner(System.in);

        CardShuffler shuffler = new CardShuffler();
        shuffler.printCards("Unsorted Card List");
        
        boolean keepGoing = true;
        while (keepGoing) {
            
            System.out.println(question);
            int inputInt = -1;
            try {
                String input = scanner.nextLine();
                inputInt = Integer.parseInt(input);                
            } catch (NumberFormatException e) {
                System.out.println("Invalid selection\n\n");
            }
            
            if (inputInt == 3) {
                keepGoing = false;
            } else if (inputInt == 1) {
                Card randomCard = shuffler.drawRandomCard();
                System.out.println("Random card : " + randomCard + "\n\n");
            } else if (inputInt == 2) {
                shuffler.shuffleCards();
                shuffler.printCards("Shuffled Card List");
            } else {
                System.out.println("Invalid selection\n\n");
            }
            
        }
    }
}
